"use strict"

/**
 * Affichage d'un objet personnage
 * @param {object} objet 
 */
var affichage_perso = function(objet){
    // Affichage de l'objet
    for (const prop in objet) {
        if (objet.hasOwnProperty(prop)) {
            const element = objet[prop];
            console.log(prop + " : " + element);
        }
    }
}

module.exports= affichage_perso;