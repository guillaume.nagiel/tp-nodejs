"use strict"
// import du module permettant la saisi d'un utilisateur
let prompt = require('prompt-sync')({sigint: true});
// import du module permettant la lecture et l'écriture d'un fichier
let fs = require("fs");

/**
 * Affichage de chaque objet du tableau personnages
 * @param {Array<object>} personnages 
 */
let sup_perso = function(personnages){
    let choix;
    let listePersos = "Quelle personnage voulez vous supprimer ?\n";
    // Parcour du tableau pour générer une liste des personnages
    for (let i = 0; i < personnages.length; i++) {
        const element = personnages[i];
        listePersos = listePersos + (i+1) + " : " + element.nom + "\n";
    }
    console.log(listePersos); // Affichage des instructions
    choix = Number(prompt()) - 1; // Choix du personnage à supprimer en saisissant son numéro

    if (!isNaN(parseFloat(choix)) && isFinite(choix)) { // Vérification qu'on est bien saisie un nombre
        personnages.splice(choix, 1); // Suppression du personnage
        console.log("Le personnage a bien été supprimé");
    } else {
        console.log("Saisissez un chiffre");
    }


    // enregistrement du tableau dans un fichier
    fs.writeFileSync("personnages.json", JSON.stringify(personnages, undefined, 4), "UTF-8");

}

module.exports= sup_perso;