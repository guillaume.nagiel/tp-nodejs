 "use strict"
// import du module permettant la saisi d'un utilisateur
let prompt = require('prompt-sync')();
// import du module permettant la lecture et l'écriture d'un fichier
var fs = require("fs");
// import des modules personalisés
var affichage_perso = require("./affichage_perso.js");

/**
 * Création d'un personnage et ajout au tableau
 * @param {Array<object>} personnages 
 */
var creation_perso = function(personnages){
    // Déclaration des objets et des variables
    let personnage = {};

    let nom;
    let classe;
    let arme;
    let endurance;
    let force;
    let agilite;
    let intelligence;

    // Attribution des valeurs aux variables
    console.log("Vous allez maintenant créer un personnage");
    nom = prompt("Veuillez saisir un nom : ");
    classe = prompt("Veuillez saisir une classe : ");
    arme = prompt("Veuillez saisir une arme : ");
    endurance = prompt("Veuillez saisir l'endurance : ");
    force = prompt("Veuillez saisir la force : ");
    agilite = prompt("Veuillez saisir l'agilite : ");
    intelligence = prompt("Veuillez saisir l'intelligence : ");

    // Création de l'objet personnage
    personnage.nom = nom;
    personnage.classe = classe;
    personnage.arme = arme;
    personnage.endurance = endurance;
    personnage.force = force;
    personnage.agilite = agilite;
    personnage.intelligence = intelligence;

    // Affichage du personnage
    console.log("Voici votre nouveau personnage : ");
    affichage_perso(personnage);    
    
    // Ajout au tableau
    personnages.push(personnage);

    // enregistrement du tableau dans un fichier
    fs.writeFileSync("personnages.json", JSON.stringify(personnages, undefined, 4), "UTF-8");
}

module.exports= creation_perso;
