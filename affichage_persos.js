"use strict"
// import des modules personalisés
let affichage_perso = require("./affichage_perso.js");

/**
 * Affichage de chaque objet du tableau personnages
 * @param {Array<object>} personnages 
 */
let affichage_persos = function(personnages){
    // Parcour du tableau
    for (let i = 0; i < personnages.length; i++) {
        const element = personnages[i];
        console.log("------------------------------");
        affichage_perso(element); // Appel de la fonction affichant un objet personnage
    }
    console.log("------------------------------");
}

module.exports= affichage_persos;