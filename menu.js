"use strict"
// import du module permettant la saisi d'un utilisateur
let prompt = require('prompt-sync')();
// import du module permettant la lecture et l'écriture d'un fichier
var fs = require("fs");

// import des modules personalisés
let creation_perso = require("./creation_perso.js");
var affichage_persos = require("./affichage_persos.js");
var sup_perso = require("./sup_perso.js");

// Déclaration des objets et des variables
let quit = false;
let choix;
let personnages = [];

// Lecture du fichier et transformation en tableau
if (fs.existsSync("personnages.json")) {
    var contenu = fs.readFileSync("personnages.json", "UTF-8");
    if (contenu != undefined) {
        personnages = JSON.parse(contenu);
    }
}

while (!quit) {
    console.log("Sélectionnez une action\n1: Créer un personnage\n2: Sélectionner un personnage\n3: Supprimer un personnage\n4: Voir tous ses personnages\n5: Quitter l’application : ");
    choix = prompt(); 

    switch (choix) {
        case "1":
            console.log("------Créer un personnage------");
            creation_perso(personnages);
            break;
        case "2":
            console.log("------Sélectionner un personnage------");
            break;
        case "3":
            console.log("------Supprimer un personnage------");
            sup_perso(personnages);
            break;
        case "4":
            console.log("------Voir tous ses personnages------");
            affichage_persos(personnages);
            break;
        case "5":
            quit = true;
            break;
    
        default:
            console.log("saisissez un choix valide");
            break;
    }
}